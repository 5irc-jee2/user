package fr.cpe.jee2.user.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.cpe.jee2.user.model.UserModel;

public interface UserRepository extends CrudRepository<UserModel, Integer> {
	
	List<UserModel> findByLoginAndPwd(String login,String pwd);

}
