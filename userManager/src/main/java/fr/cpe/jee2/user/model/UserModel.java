package fr.cpe.jee2.user.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.cpe.jee2.card.dto.CardModelDTO;
import fr.cpe.jee2.dto.UserDisplayDTO;
import fr.cpe.jee2.dto.UserModelDTO;

import java.io.Serializable;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class UserModel implements Serializable {

	private static final long serialVersionUID = 2733795832476568049L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String login;
	private String pwd;
	private float account;
	private String lastName;
	private String surName;
	private String email;

	private transient Set<CardModelDTO> cardList = new HashSet<>();

	public UserModel() {
		this.login = "";
		this.pwd = "";
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
	}

	public UserModel(String login, String pwd) {
		super();
		this.login = login;
		this.pwd = pwd;
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
	}

	public UserModelDTO toUserModelDTO(){
		UserModelDTO userModelDTO = new UserModelDTO();
		userModelDTO.setId(this.id);
		userModelDTO.setAccount(this.account);
		userModelDTO.setEmail(this.email);
		userModelDTO.setLastName(this.lastName);
		userModelDTO.setLogin(this.login);
		userModelDTO.setPwd(this.pwd);
		userModelDTO.setSurName(this.surName);
		Set<Integer> listCard = new HashSet<>();
		for(CardModelDTO cardModel : this.cardList){
			listCard.add(cardModel.getId());
		}
		userModelDTO.setCardList(listCard);
		return userModelDTO;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Set<CardModelDTO> getCardList() {
		return cardList;
	}

	public void setCardList(Set<CardModelDTO> cardList) {
		this.cardList = cardList;
	}

	public void addAllCardList(Collection<CardModelDTO> cardList) {
		this.cardList.addAll(cardList);
	}


	public void addCard(CardModelDTO card) {
		this.cardList.add(card);
		card.setUserId(this.id);
	}

	private boolean checkIfCard(CardModelDTO c){
		for(CardModelDTO c_c: this.cardList){
			if(c_c.getId()==c.getId()){
				return true;
			}
		}
		return false;
	}

	public float getAccount() {
		return account;
	}

	public void setAccount(float account) {
		this.account = account;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		final StringBuilder stringBuilder = new StringBuilder("UserModel{");
		stringBuilder.append("id=").append(id);
		stringBuilder.append(", login='").append(login).append('\'');
		stringBuilder.append(", pwd='").append(pwd).append('\'');
		stringBuilder.append(", account=").append(account);
		stringBuilder.append(", lastName='").append(lastName).append('\'');
		stringBuilder.append(", surName='").append(surName).append('\'');
		stringBuilder.append(", email='").append(email).append('\'');
		stringBuilder.append(", cardList=").append(cardList);
		stringBuilder.append('}');
		return stringBuilder.toString();
	}
}
