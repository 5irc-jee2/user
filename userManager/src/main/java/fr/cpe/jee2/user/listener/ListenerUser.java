package fr.cpe.jee2.user.listener;

import fr.cpe.jee2.dto.EnveloppeDTO;
import fr.cpe.jee2.receiver.controller.BusListener;
import org.springframework.stereotype.Component;

@Component
public class ListenerUser extends BusListener {
    @Override
    public void parseData(EnveloppeDTO pEnveloppeDTO) {
        //TODO implement parser
        System.out.println(pEnveloppeDTO.toString());
    }
}
