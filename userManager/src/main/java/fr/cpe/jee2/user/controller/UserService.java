package fr.cpe.jee2.user.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.cpe.jee2.card.constantes.ConstanteCard;
import fr.cpe.jee2.dto.EnveloppeDTO;
import fr.cpe.jee2.dto.constantes.ConstanteUser;
import fr.cpe.jee2.emitter.controller.BusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.cpe.jee2.user.model.UserModel;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BusService busService;

	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}

	public Optional<UserModel> getUser(String id) {
		return userRepository.findById(Integer.valueOf(id));
	}

	public Optional<UserModel> getUser(Integer id) {
		return userRepository.findById(id);
	}

	public void addUser(UserModel user) {
		System.out.println("Ajout d'un utilisateur : " + user.toString());
		// needed to avoid detached entity passed to persist error
		userRepository.save(user);

		//On prepare le message pour la queue
		EnveloppeDTO enveloppeDTO = new EnveloppeDTO();
		enveloppeDTO.setMethodName(ConstanteCard.METHOD_RANDOM_CARD);
		enveloppeDTO.addParameters(ConstanteCard.PARAM_RAND_CARD_NB, 5);
		enveloppeDTO.addParameters(ConstanteCard.PARAM_APPLICATION_REPONSE,
				ConstanteUser.ID_APPLICATION);
		System.out.println("Enveloppe  : " + enveloppeDTO.toString());
		busService.sendMsg(enveloppeDTO, ConstanteCard.QUEUE_NAME);

		//TODO ajouter les cartes à l'utilisateur
		userRepository.save(user);
	}

	public void updateUser(UserModel user) {
		userRepository.save(user);

	}

	public void deleteUser(String id) {
		userRepository.deleteById(Integer.valueOf(id));
	}

	public List<UserModel> getUserByLoginPwd(String login, String pwd) {
		List<UserModel> ulist=null;
		ulist=userRepository.findByLoginAndPwd(login,pwd);
		return ulist;
	}

}
