package fr.cpe.jee2.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class UserModelDTO implements Serializable {

	private static final long serialVersionUID = 2733795832476568049L;

	private Integer id;
	private String login;
	private String pwd;
	private float account;
	private String lastName;
	private String surName;
	private String email;

	private Set<Integer> cardList = new HashSet<>();

	public UserModelDTO() {
		this.login = "";
		this.pwd = "";
		this.lastName = "lastname_default";
		this.surName = "surname_default";
		this.email = "email_default";
	}

	public UserModelDTO(String login, String pwd) {
		super();
		this.login = login;
		this.pwd = pwd;
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
	}
	public Integer getId() {
		return id;
	}

	public String getLogin() {
		return login;
	}

	public String getPwd() {
		return pwd;
	}

	public Set<Integer> getCardList() {
		return cardList;
	}

	public float getAccount() {
		return account;
	}

	public String getLastName() {
		return lastName;
	}

	public String getSurName() {
		return surName;
	}

	public String getEmail() {
		return email;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public void setAccount(float account) {
		this.account = account;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setCardList(Set<Integer> cardList) {
		this.cardList = cardList;
	}

	@Override
	public String toString() {
		final StringBuilder stringBuilder = new StringBuilder("UserModelDTO{");
		stringBuilder.append("id=").append(id);
		stringBuilder.append(", login='").append(login).append('\'');
		stringBuilder.append(", pwd='").append(pwd).append('\'');
		stringBuilder.append(", account=").append(account);
		stringBuilder.append(", lastName='").append(lastName).append('\'');
		stringBuilder.append(", surName='").append(surName).append('\'');
		stringBuilder.append(", email='").append(email).append('\'');
		stringBuilder.append(", cardList=").append(cardList);
		stringBuilder.append('}');
		return stringBuilder.toString();
	}
}
