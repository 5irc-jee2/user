package fr.cpe.jee2.dto;

import java.util.HashSet;
import java.util.Set;

public class UserDisplayDTO {
	private Integer id;
	private String login;
	private String pwd;
    private Set<Integer> cardList = new HashSet<>();
    
    public UserDisplayDTO(UserModelDTO user) {
		this.id = user.getId();
		this.login = user.getLogin();
		this.pwd = user.getPwd();
		cardList.addAll(user.getCardList());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Set<Integer> getCardList() {
		return cardList;
	}

	public void setCardList(Set<Integer> cardList) {
		this.cardList = cardList;
	}

	@Override
	public String toString() {
		final StringBuilder stringBuilder = new StringBuilder("UserDisplayDTO{");
		stringBuilder.append("id=").append(id);
		stringBuilder.append(", login='").append(login).append('\'');
		stringBuilder.append(", pwd='").append(pwd).append('\'');
		stringBuilder.append(", cardList=").append(cardList);
		stringBuilder.append('}');
		return stringBuilder.toString();
	}
}
