package fr.cpe.jee2.dto.constantes;

public class ConstanteUser {
    public static final String ID_APPLICATION = "USER_APPLICATION";

    public static final String QUEUE_NAME = "queue_user";
}
